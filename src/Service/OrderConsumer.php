<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Service\OrderConsumer\Reader;

/**
 * Class OrderConsumer
 * @package App\Service
 */
class OrderConsumer
{
    private $reader;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->reader = new Reader($entityManager);
    }


    /**
     * @param string $url
     */
    public function createFromUrl(string $url)
    {
        // Consommation de l'API

        try {
         
            $response = @file_get_contents($url, true);

            if ($response === FALSE) {
                throw new NotFoundHttpException("Api not found, check the lengow_test_host parameter in the .env file");
            }

        } catch (NotFoundHttpException $e) {

            echo $e->getMessage();

        }

        // Récuperation du header de la réponse

        try {

            $headerArray = get_headers($url, 1);

            if ($headerArray === FALSE) {
                throw new NotFoundHttpException("Header not found, could not retrieve data");
            }

        } catch (NotFoundHttpException $e) {

            echo $e->getMessage();
        }

        //  Vérification du format du header et ajout en base de données

        if (!$this->reader->readData($response, $headerArray['Content-Type']))
            echo 'Unsupported format, modify the App\Service\OrderConsumer\Reader class to add new formats types';
    }
}