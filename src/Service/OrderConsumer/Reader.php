<?php

namespace App\Service\OrderConsumer;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class Reader
 * @package App\Service\OrderConsumer
 */

class Reader {

    private $entityManager;

    // Definir ici d'autre type de headers

    private const HEADER_XML  = "text/xml; charset=UTF-8";
    private const HEADER_JSON = "application/json";
    
    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $data
     * @param string $headerType
     * @return bool
     */
    public function readData($data, $headerType) {

        // Ajouter dans le switch d'autres types de parsers

        switch ($headerType) {

            case self::HEADER_XML:
                (new ParserXML($this->entityManager))->parseNewOrder($data);
                break;

            case self::HEADER_JSON:
                (new ParserJSON($this->entityManager))->parseNewOrder($data);
                break;

            default :
                return false;
        }
    
        return true;
    }
}