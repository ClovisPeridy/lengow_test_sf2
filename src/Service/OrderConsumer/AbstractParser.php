<?php

namespace App\Service\OrderConsumer;

use Doctrine\ORM\EntityManagerInterface;


/**
 * Abstract Class AbstractParser
 * @package App\Service\OrderConsumer
 */

abstract class AbstractParser implements ParserInterface {

    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $data
     */
    abstract function parseNewOrder($data);
}