<?php

namespace App\Service\OrderConsumer;

use App\Entity\Customer;
use App\Entity\Order;
use App\Entity\OrderLine;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ParserXML
 * @package App\Service\OrderConsumer
 */

class ParserXML extends AbstractParser {

    /**
     * @param string $data
     */
    public function parseNewOrder($data) {

        $newOrders = simplexml_load_string($data);

        // Enregistrement en BDD

        $customerRepository = $this->entityManager->getRepository(Customer::class);
        
        foreach ($newOrders as $newOrder) {

            // Si un customer existe avec cet ID, créer une nouvelle Order
            if ($newOrder && $customerRepository->find($newOrder->customer_id)) {

                $entryOrder = new Order();
        
                $entryDate = date_create_from_format('Y-m-d H:i:s.u', $newOrder->date->date);
                         
                if ($entryDate != false)
                    $entryOrder->setCreatedAt($entryDate);
                else
                    $entryOrder->setCreatedAt(new \DateTime());
                             
                $entryOrder->setStatus(Order::STATUS_NEW);
                $entryOrder->setCustomer($customerRepository->find($newOrder->customer_id));
         
                // Créer les OrderLines et les ajouté a l'Order actuelle
                foreach ($newOrder->orderlines->{'item'} as $orderline) {

                    $entryLine = new Orderline();
                                
                    $entryLine->setProduct($orderline->product);
                    $entryLine->setQuantity($orderline->quantity);
                    $entryLine->setPrice($orderline->price);
                    $entryLine->setOrder($entryOrder);
         
                    $entryOrder->addOrderLine($entryLine);
                }
         
                $this->entityManager->persist($entryOrder);
            }
        }

        $this->entityManager->flush();

    }
}