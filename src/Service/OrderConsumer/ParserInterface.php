<?php

namespace App\Service\OrderConsumer;

/**
 * Interface ParserInterface
 * @package App\Service\OrderConsumer
 */

interface ParserInterface {

    public function parseNewOrder($data);

}